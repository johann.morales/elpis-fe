import axios from 'axios';
import {message} from 'antd';


const url = 'http://200.16.7.151:80';

export const requestAPI = axios.create({
  baseURL: url,
  timeout: 1000
});

requestAPI.interceptors.request.use((config) => {
  const token = localStorage.getItem('jwt');
  config.headers = {
    'Access-Control-Allow-Headers': 'x-access-token',
    'x-access-token': `${token}`
  };
  return config;
})

requestAPI.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  message.error( 'Error de conexion con el servidor');
  return Promise.reject(error);
});

export const authAPI = axios.create({
  baseURL: url
});

export function download(id) {
  requestAPI.get('general/descargarArchivo?id=' + id, {
    responseType: 'arraybuffer'
  }).then(response => {
    console.log(JSON.stringify(response.headers, null, 2))
    require('downloadjs')(response.data, response.headers['file-name'], response.headers['content-type']);
  })
}



