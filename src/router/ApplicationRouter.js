import React from 'react';
import {Switch, Route} from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';
import SimpleRoute from './SimpleRoute';
import LayoutBasic from '../layouts/LayoutBasic';
import routes from '../routes';
import Error403 from '../views/_err/403';
import Error404 from '../views/_err/404';
import Error500 from '../views/_err/500';
import Redirector from './Redirector';

const authorizedRoutes = routes.filter(route => route.authorization);
const openRoutes = routes.filter(route => ! route.authorization);

export default class ApplicationRouter extends React.Component {

  render() {
    return (
        <Switch>
          <Route exact path="/" component={Redirector}/>
          {openRoutes.map(route => {
            const View = route.component.default;
            const Layout = route.layout.default;
            return (
                <SimpleRoute
                    exact
                    key={route.path.name}
                    path={route.path.name}
                    layout={Layout}
                    component={View}>
                </SimpleRoute>
            )
          })}
          {authorizedRoutes.map(route => {
            const View = route.component.default;
            return (
                <ProtectedRoute
                    layout={LayoutBasic}
                    key={route.path.name}
                    roles={route.roles}
                    path={route.path.name}
                    component={View}>
                </ProtectedRoute>
            )
          })}
          <Route exact path="/error/403" component={Error403}></Route>
          <Route exact path="/error/404" component={Error404}></Route>
          <Route exact path="/error/500" component={Error500}></Route>
          <Route component={Error404}></Route>
        </Switch>
    )
  }
}