import React from 'react';
import PropTypes from 'prop-types';
import {currentRole} from '../util';
import {Redirect} from 'react-router-dom';
import SimpleRoute from './SimpleRoute';

export default class ProtectedRoute extends React.Component {

  render() {
    const role = currentRole();

    return (
        <div>
          {
            role && this.props.roles.includes(role) ?
                <SimpleRoute layout={this.props.layout} exact path={this.props.path} component={this.props.component}/> :
                <Redirect to="/error/403"/>
          }
        </div>
    )
  }
}

ProtectedRoute.propTypes = {
  roles: PropTypes.array.isRequired,
  path: PropTypes.string.isRequired,
  exact: PropTypes.bool
}