import React from 'react';
import {Redirect} from 'react-router-dom';
import {getUser} from '../util';

export default class Redirector extends React.Component {

  render() {
    if(getUser() !== null){
      return <Redirect to="/dashboard"/>
    }else {
      return <Redirect to="/login"/>
    }
  }

}