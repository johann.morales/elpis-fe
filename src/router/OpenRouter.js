import React from 'react';
import {Switch, Route} from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';
import routes from '../routes';
import Error403 from '../views/_err/403';
import Error404 from '../views/_err/404';

const authorizedRoutes = routes.filter(route => route.authorization);

export default class AuthorizedRouter extends React.Component {

  render() {
    return (
        <div>
            {authorizedRoutes.map(route => {
              const View = route.component.default;
              return (
                  <Route
                      key={route.path.name}
                      roles={route.roles}
                      path={'/asd' + route.path.name}
                      component={View}>
                  </Route>
              )
            })}
        </div>
    )
  }
}