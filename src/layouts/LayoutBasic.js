import React from 'react';
import DocumentTitlte from 'react-document-title';
import {ContainerQuery} from 'react-container-query';
import {Layout} from 'antd';
import {TheFooter, TheHeader, TheSider} from './components';
import {enquireScreen} from 'enquire-js';
import classNames from 'classnames';
import logo from '../assets/logo.svg';

const { Content, Header, Footer } = Layout;

const query = {
  'screen-xs': {
    maxWidth: 575
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199
  },
  'screen-xl': {
    minWidth: 1200
  }
};

let isMobile;
enquireScreen(b => {
  isMobile = b;
});

export default class LayoutBasic extends React.Component {

  state = {
    collapsed : true,
    isMobile
  }

  constructor() {
    super();
  }

  componentDidMount() {
    this.enquireHandler = enquireScreen(mobile => {
      this.setState({
        isMobile: mobile,
        collaped: true
      });
    });
    if(localStorage.getItem('isCollapsed') === 'true'){
      this.handleMenuCollapse(true);
    }
  }

  componentWillUnmount() {
    // unenquireScreen(this.enquireHandler);
  }

  getPageTitle() {
    return 'hola mundo';
  }

  handleMenuCollapse = (collapsed) => {
    this.setState({ collapsed: collapsed }, () => {
      localStorage.setItem('isCollapsed',  collapsed);
    });
  };

  handleMenuClick = ({ key }) => {
    switch (key) {
      case 'logout':
        localStorage.removeItem('jwt');
        localStorage.removeItem('u');
        this.props.history.push('/');
        return;
      default:
        console.log('pendiente');
    }
  };


  render() {
    const currentUser = {
      name: 'Johann Morales',
      avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png'
    }
    const layout = (
        <Layout>
          <TheSider
              logo={logo}
              Authorized={[]}
              menuData={[]}
              collapsed={this.state.collapsed}
              isMobile={this.state.isMobile}
              onCollapse={this.handleMenuCollapse}
          />
          <Layout>
            <Header style={{ padding: 0 }}>
              <TheHeader
                  logo={logo}
                  currentUser={currentUser}
                  collapsed={this.state.collapsed}
                  isMobile={this.state.isMobile}
                  onCollapse={this.handleMenuCollapse}
                  onMenuClick={this.handleMenuClick}
                  onNoticeVisibleChange={this.handleNoticeVisibleChange}
              />
            </Header>
            <Content style={{ margin: '24px 24px 0', height: '100%' }}>
              {this.props.children}
            </Content>
            <Footer style={{ padding: 0 }}>
              <TheFooter/>
            </Footer>
          </Layout>
        </Layout>
    );
    return (
        <DocumentTitlte title={this.getPageTitle()}>
          <ContainerQuery query={query}>
            {params => <div className={classNames(params)}>
              {layout}
              </div>}
          </ContainerQuery>
        </DocumentTitlte>
    )
  }

}