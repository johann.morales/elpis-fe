import React from 'react';
import styles from './TheFooter.module.less';

export default class TheFooter extends React.Component {
  render() {
    return (
        <div className={styles.globalFooter}>
          <div className={styles.copyright}> Kairos @ 2018 </div>
        </div>
    )
}

}