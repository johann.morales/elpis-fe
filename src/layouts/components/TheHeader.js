import React from 'react';
import {Menu, Icon, Spin, Dropdown, Avatar, Divider} from 'antd';
import {Link} from 'react-router-dom';
import styles from './TheHeader.module.less';
import {getUser} from '../../util';

const user = getUser();

export default class TheHeader extends React.Component {

  toggle = () => {
    const { collapsed, onCollapse } = this.props;
    onCollapse(! collapsed);
    // this.triggerResizeEvent();
  };

  triggerResizeEvent() {
    // const event = document.createEvent('HTMLEvents');
    // event.initEvent('resize', true, false);
    // window.dispatchEvent(event);
  }

  render() {

    const {
      currentUser = {},
      collapsed,
      isMobile,
      logo,
      onMenuClick,
    } = this.props;

    const menu = (
        <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
          <Menu.Item key="logout">
            <Icon type="logout"/>Salir
          </Menu.Item>
        </Menu>
    );

    return (
        <div className={styles.header}>
          {isMobile && [
            <Link to="/" className={styles.logo} key="logo">
              <img src={logo} alt="logo" width="32" />
            </Link>,
            <Divider type="vertical" key="line" />,
          ]}
          <Icon
              className={styles.trigger}
              type={collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
          />
          <div className={styles.right}>
            {currentUser.name ? (
                <Dropdown overlay={menu}>
                  <span className={`${styles.action} ${styles.account}`}>
                    <Avatar size="small" className={styles.avatar} src={currentUser.avatar}/>
                    <span className={styles.name}> {user.nombre} </span>
                  </span>
                </Dropdown>
            ) : (
                <Spin size="small" style={{ marginLeft: 8 }}/>
            )}
          </div>
        </div>
    )
  }
}