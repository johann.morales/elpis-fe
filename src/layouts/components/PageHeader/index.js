import React, { PureComponent, createElement } from 'react';
import { Breadcrumb, Tabs } from 'antd';
import classNames from 'classnames';
import styles from './index.module.less';
const { TabPane } = Tabs;

export default class PageHeader extends PureComponent {

  state = {
    breadcrumb: null,
  };

  componentDidMount() {
    this.getBreadcrumbDom();
  }

  componentWillReceiveProps() {
    this.getBreadcrumbDom();
  }
  componentDidUpdate(preProps) {
    if (preProps.tabActiveKey !== this.props.tabActiveKey) {
      this.getBreadcrumbDom();
    }
  }

  getBreadcrumbDom = () => {
    const breadcrumb = this.conversionFromProps();
    this.setState({
      breadcrumb,
    });
  };
  // Generated according to props
  conversionFromProps = () => {
    const { breadcrumbList=[], linkElement = 'a' } = this.props;
    return (
      <Breadcrumb className={styles.breadcrumb} separator={">"}>
        {breadcrumbList.map(item => (
          <Breadcrumb.Item key={item.title}>
            {item.href
              ? createElement(
                  linkElement,
                  {
                    [linkElement === 'a' ? 'href' : 'to']: item.href,
                  },
                  item.title
                )
              : item.title}
          </Breadcrumb.Item>
        ))}
      </Breadcrumb>
    );
  };

  render() {
    const {
      title,
      logo,
      action,
      content,
      extraContent,
      tabList,
      className,
      tabActiveKey,
      tabDefaultActiveKey,
      tabBarExtraContent,
    } = this.props;

    const clsString = classNames(styles.pageHeader, className);
    const activeKeyProps = {};
    if (tabDefaultActiveKey !== undefined) {
      activeKeyProps.defaultActiveKey = tabDefaultActiveKey;
    }
    if (tabActiveKey !== undefined) {
      activeKeyProps.activeKey = tabActiveKey;
    }

    return (
        <div className={clsString}>
          {this.state.breadcrumb}
          <div className={styles.detail}>
            {logo && <div className={styles.logo}>{logo}</div>}
            <div className={styles.main}>
              <div className={styles.row}>
                {title && <h1 className={styles.title}>{title}</h1>}
                {action && <div className={styles.action}>{action}</div>}
              </div>
              <div className={styles.row}>
                {content && <div className={styles.content}>{content}</div>}
                {extraContent && <div className={styles.extraContent}>{extraContent}</div>}
              </div>
            </div>
          </div>
          {tabList &&
          tabList.length && (
              <Tabs
                  className={styles.tabs}
                  {...activeKeyProps}
                  onChange={this.onChange}
                  tabBarExtraContent={tabBarExtraContent}
              >
                {tabList.map(item => <TabPane tab={item.tab} key={item.key} />)}
              </Tabs>
          )}
        </div>
    );
  }
}
