import React from 'react';
import {ContainerQuery} from 'react-container-query';
import {Layout} from 'antd';
import classNames from 'classnames';
import styles from './LayoutBlank.module.less'

const query = {
  'screen-xs': {
    maxWidth: 575
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199
  },
  'screen-xl': {
    minWidth: 1200
  }
};

export default class LayoutBasic extends React.Component {


  render() {
    return (
        <ContainerQuery query={query}>
          {params =>
              <div className={classNames(params)}>
                <Layout className={styles.login}>
                  {this.props.children}
                </Layout>
              </div>
          }
        </ContainerQuery>
    )
  }

}