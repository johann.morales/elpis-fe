import React from 'react';
import {Card, Icon, Table, Tag, Input} from 'antd';
import styles from './index.module.less';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import DocumentTitle from 'react-document-title';
import {requestAPI} from '../../api';

const Search = Input.Search;

const columns = [{
  key: 'foto',
  title: '',
  align: 'right',
  width: '100px',
  render: (record) => {
    return (
        <div>
          <img
              className={styles.image}
              src="https://www.yourbusinessfreedom.com.au/wp-content/uploads/2017/02/unknown-profile-1.jpg"
              alt="foto"
          />
        </div>
    )
  }
}, {
  key: 'nombre',
  title: 'Nombre',
  render: (record) => {
    return (
        <div>
          <b> {record.nombre} </b>
          <small style={{ display: 'block' }}> Código {record.codigo}</small>
        </div>
    )
  }
}, {
  key: 'seccion',
  title: 'Seccion',
  align: 'center',
  render: (r) => {
    return (
        <div>
          <span> Secc. de {r.seccion} </span>
          <small style={{ display: 'block' }}> Dpto. de {r.departamento}</small>
        </div>
    )
  }
}, {
  key: 'contacto',
  title: 'Información de Contacto',
  align: 'center',
  render: (r) => {
    return (
        <div>
          <span style={{ display: 'block' }}> <Icon type="phone"/> {r.telefono} </span>
          <span style={{ display: 'block', marginTop: '3px' }}> <Icon type="mobile"/> (+51) 991 142 846 </span>
          <span style={{ display: 'block', marginTop: '3px' }}> <Icon type="mail"/> {r.correo_pucp}</span>
        </div>
    )
  }
}, {
  key: 'tipo',
  title: 'Tipo',
  align: 'center',
  render: (r) => {
    return (
        <div>
          {r.descripcion === 'TC' ?
              <Tag color="blue">Tiempo Completo</Tag> :
              <Tag color="volcano">Tiempo Parcial</Tag>
          }
        </div>
    )
  }
}];

export default class Profesores extends React.Component {

  constructor() {
    super();
    this.state =
        {
          profesoresAux: [],
          profesores: [],
        }
  }

  componentDidMount() {
    requestAPI.get('general/listaDocente')
        .then(response => {
          this.setState({ profesores: response.data.docentes, profesoresAux: response.data.docentes });
        })
        .catch(error => {
        });
  }

  buscar = (text) => {
    let aux = this.state.profesores;

    if (text && text !== '') {//la lista no esta filtrada
      aux = this.state.profesores.filter((d) => {
        return d.nombre.toUpperCase().indexOf(text.toUpperCase()) !== - 1
      });
    }
    this.setState({
      profesoresAux: aux
    })
  }

  render() {
    const { match, history } = this.props;
    const breadcrumbList = [];

    return (
        <DocumentTitle title="Elpis - Profesores">
          <PageHeaderLayout title="Profesores" breadcrumbList={breadcrumbList}>
            <Card bordered={false}>
              <Search
                  placeholder="Buscar"
                  className={styles.search}
                  onSearch={this.buscar}/>
              <Table
                  rowKey="codigo"
                  columns={columns}
                  dataSource={this.state.profesoresAux}
                  pagination={{ pageSize: 5 }}
                  onRow={(record) => {
                    return {
                      onClick: () => {
                        history.push(`${match.url}/${record.codigo}`)
                      }
                    };
                  }}/>
            </Card>
          </PageHeaderLayout>
        </DocumentTitle>
    )
  }
}