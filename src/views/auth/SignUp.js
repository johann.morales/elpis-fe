import React from 'react';
import styles from './SignUp.module.less';
import DocumentTitle from 'react-document-title';
import {Steps, Button, message, Card, Row, Col, Form, Input, Tooltip, Icon, Cascader, Select, Checkbox, AutoComplete} from 'antd';

const Step = Steps.Step;
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;


const residences = [{
  value: 'zhejiang',
  label: 'Zhejiang',
  children: [{
    value: 'hangzhou',
    label: 'Hangzhou',
    children: [{
      value: 'xihu',
      label: 'West Lake'
    }]
  }]
}, {
  value: 'jiangsu',
  label: 'Jiangsu',
  children: [{
    value: 'nanjing',
    label: 'Nanjing',
    children: [{
      value: 'zhonghuamen',
      label: 'Zhong Hua Men'
    }]
  }]
}];


export class SignUp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      confirmDirty: false,
      autoCompleteResult: []
    };
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { current } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    const steps = [{
      title: 'Datos Generales',
      content: 'First-content'
    }, {
      title: 'Rol',
      content: (
          <FormItem
              {...formItemLayout}
              label="E-mail"
          >
            {getFieldDecorator('email', {
              rules: [{
                type: 'email', message: 'The input is not valid E-mail!'
              }, {
                required: true, message: 'Please input your E-mail!'
              }]
            })(
                <Input/>
            )}
          </FormItem>
      )
    }, {
      title: 'Autenticación',
      content: 'Last-content'
    }];

    return (
        <DocumentTitle title="Elpis - Registro">
          <div>
            <Form>
              <Card bordered={false} className={styles.wizard}>
                <div>
                  <Steps current={current}>
                    {steps.map(item => <Step key={item.title} title={item.title}/>)}
                  </Steps>
                  <div className={styles['steps-content']}>
                    {steps[this.state.current].content}
                  </div>
                  <div className={styles['steps-action']} style={{ textAlign: 'right' }}>
                    {
                      this.state.current > 0
                      &&
                      <Button style={{ marginRight: 8 }} onClick={() => this.prev()}>
                        Anterior
                      </Button>
                    }
                    {
                      this.state.current < steps.length - 1
                      &&
                      <Button type="primary" onClick={() => this.next()}>Siguiente</Button>
                    }
                    {
                      this.state.current === steps.length - 1
                      &&
                      <Button type="primary" onClick={() => message.success('Processing complete!')}>Listo!</Button>
                    }
                  </div>
                </div>
              </Card>
            </Form>
          </div>
        </DocumentTitle>
    )
  }
}

export default Form.create()(SignUp);
