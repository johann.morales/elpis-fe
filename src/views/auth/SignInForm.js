import React from 'react';
import {Form, Icon, Input, Button, message} from 'antd';
import {authAPI} from '../../api';

const FormItem = Form.Item;

class RegistroGeneralForm extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (! err) {
        authAPI.post('auth/login', values)
            .then(resp => {
              if (resp.data.auth) {
                localStorage.setItem('jwt', resp.data.token);
                localStorage.setItem('u', btoa(JSON.stringify(resp.data.user)));
                message.success('Bienvenido!');
                window.location.href = "/";
              } else {
                message.error('Credenciales no válidos')
              }
            })
            .catch(err => {
                message.error('Credenciales no válidos')
            });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
        <Form onSubmit={this.handleSubmit} className="login-form">
          <FormItem>
            {getFieldDecorator('codigo', {
              rules: [{ required: true, message: 'Por favor ingrese su nombre de usuario' }]
            })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }}/>} placeholder="Usuario"/>
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Por favor ingrese su contraseña' }]
            })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }}/>} type="password" placeholder="Contraseña"/>
            )}
          </FormItem>
          <FormItem style={{marginBottom:'0px'}}>
            <Button type="primary" htmlType="submit" className="login-form-button" style={{marginRight: '10px', width: '30%'}}>
              Ingresar
            </Button>
            O <a href="/signup">regístrate!</a>
          </FormItem>
        </Form>
    )
  }
}

export default Form.create()(RegistroGeneralForm);