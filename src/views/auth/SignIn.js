import React from 'react';
import styles from './SignIn.module.less';
import DocumentTitle from 'react-document-title';
import Form from './SignInForm';
import {Card} from 'antd';
import {getUser} from '../../util';
import {Redirect} from 'react-router-dom';

export default class SignIn extends React.Component {

  render() {
    return (
        <div>
          {getUser() !== null ? <Redirect to="/"/> : null}
          <DocumentTitle title="Elpis - Iniciar Sesión">
              <Card className={styles.form}>
                <Form/>
              </Card>
          </DocumentTitle>
        </div>
    )
  }
}
