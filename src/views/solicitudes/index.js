import React from 'react';
import {Card, Table, Tag} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import DocumentTitle from 'react-document-title';
import {requestAPI} from '../../api';
import {currentRole, Role} from '../../util';

export default class Postulante extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      infoDocente: {},
      ayudas: [],
      cicloSeleccionado: '',
      ciclos: [],
      selectedId: - 1,
      verDetalle: false
    }
  }

  componentDidMount() {
    this.findCicloActual();
    this.allCiclos();
  }

  getTag = (state) => {
    switch (state) {
      case 'Creada':
        return <Tag> Creado </Tag>;
      case 'Aprobado':
        return <Tag color="green"> Aprobado </Tag>;
      case 'Rechazado':
        return <Tag color="red"> Rechazado </Tag>;
      default:
        return <span></span>;
    }
  }

  findCicloActual() {
    requestAPI.get('general/cicloActual')
        .then(response => {
          this.setState({ cicloSeleccionado: response.data.cicloActual }, () => {
            this.search();
          })
        })
  }

  allCiclos() {
    requestAPI.get('general/listaCiclos')
        .then(response => {
          this.setState({ ciclos: response.data.ciclos })
        })
  }

  search = () => {
    requestAPI.get('ayudasEconomicas/ayudasEconomicas/listar', {
      params: {
        ciclo: this.state.cicloSeleccionado
      }
    }).then(response => {
      this.setState({ ayudas: response.data.ayudaEconomica });
    })
  }

  render() {
    const { match, history } = this.props;
    const breadcrumbList = [];

    const columns = [{
      key: 'codigo',
      title: 'Codigo',
      align: 'center',
      render: (r) => {
        return (
            <div>
              <b> {r.codigo_solicitud} </b>
            </div>
        )
      }
    }, {
      key: 'investigacion',
      title: 'Investigacion',
      render: (r) => {
        return (
            <div>
              <span> {r.titulo} </span>
            </div>
        )
      }
    }, {
      key: 'profesor',
      title: 'Profesor',
      render: (r) => {
        return (
            <div>
              <span> {r.profesor.nombres} {r.profesor.apellido_paterno} {r.profesor.apellido_materno} </span>
              <small style={{ display: 'block' }}> Codigo {r.profesor.codigo_profesor}</small>
            </div>
        )
      }
    }, {
      key: 'motivo',
      title: 'Motivo',
      render: (r) => {
        return (
            <div>
              <span> {r.motivo}</span>
            </div>
        )
      }
    }, {
      key: 'monto',
      title: 'Monto',
      align: 'right',
      render: (r) => {
        return (
            <div>
              <span> S/. {r.monto_otorgado.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
            </div>
        )
      }
    }, {
      key: 'estado',
      title: 'Estado',
      align: 'center',
      render: (r) => {
        return (
            <div>
              {this.getTag(r.estado)}
            </div>
        )
      }
    }];


    return (
        <DocumentTitle title="Elpis - Solicitudes Economicas">
          <PageHeaderLayout title="Solicitudes Economicas" breadcrumbList={breadcrumbList}>
            <Card bordered={false}>
              <Table
                  rowKey="id"
                  columns={columns}
                  dataSource={this.state.ayudas}
                  pagination={{ pageSize: 8 }}
                  onRow={(record) => {
                    return {
                      onClick: () => {
                        if (currentRole() === Role.JEFE_DEPARTAMENTO) {
                          history.push(`${match.url}/${record.id}/aprobar`)
                        } else {
                          history.push(`${match.url}/${record.id}/gastos`)
                        }
                      }
                    };
                  }}/>
            </Card>
          </PageHeaderLayout>
        </DocumentTitle>
    )
  }
}