import React from 'react';
import {Badge, Card, Input, Select, Table, Tag} from 'antd';
import {requestAPI} from '../../api';
import styles from './Cursos.module.less';
import DocumentTitle from 'react-document-title';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import {currentRole, Role} from '../../util';

const Option = Select.Option;
const Search = Input.Search;

const columns = [
  {
    key: 'nombre',
    title: 'Nombre',
    render: (record) => {
      return (
          <div>
            <b> {record.nombre} </b>
            <small style={{ display: 'block' }}> Código {record.codigo}</small>
          </div>
      )
    }
  }, {
    key: 'seccion',
    title: 'Sección',
    align: 'center',
    render: (r) => {
      return (
          <div>
            <span> Secc. de {r.seccion} </span>
            <small style={{ display: 'block' }}> Dpto. de {r.facultad}</small>
          </div>
      )
    }
  }, {
    key: 'creditos',
    title: '',
    align: 'center',
    render: (r) => {
      return (
          <div>
            <div>
              <Badge count={r.creditos} style={{ backgroundColor: '#52c41a' }} showZero={true}/>
              <small style={{ display: 'block', marginTop: '5px' }}> créditos</small>
            </div>
          </div>
      )
    }
  }, {
    key: 'tipo',
    title: 'Tipo',
    align: 'center',
    render: (r) => {
      return (
          <div>
            {r.tipo_curso === 'Pregrado' ?
                <Tag color="blue"> Pregrado </Tag> :
                <Tag color="volcano"> Posgrado </Tag>
            }
          </div>
      )
    }
  }
];


export default class Cursos extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      busqueda: '',
      cursoText: '',
      showDetalleCurso: false,
      selectedNombre: '',
      cursosAux: [],
      cursos: [
        {
          id: 20,
          codigo: 'INF666',
          nombre: 'Chistemas Operativos',
          seccion: 'Chinformatica',
          creditos: 20,
          tipo_curso: 'Pregrado'
        }],
      horariosSes: [
        {
          id: 1,
          horario: '0666',
          profesor: 'Viktor Khlebnikov',
          codigo_prof: '18456666',
          sesiones: 'LUN 3am-6pm v666'
        }
      ],
      secciones: []
    }
  }


  componentDidMount() {
    if ([Role.JEFE_DEPARTAMENTO, Role.ASISTENTE_DEPARTAMENTO].includes(currentRole())) {
      this.allSections();
    }
    this.all();
  }

  allSections() {
    requestAPI.get('/general/listaSeccionesDep')
        .then(response => {
          this.setState({ secciones: response.data.secciones })
        });
  }


  handleSectionChange = (value) => {
    if (value !== '0') {
      this.setState({busqueda: ''});
      this.allBySection(value);
    } else {
      this.all();
    }
  }

  all() {
    requestAPI.get('/dashboard/listaCurso')
        .then(response => {
          this.setState({
            cursos: response.data.curso,
            cursosAux: response.data.curso
          })
        });
  }

  allBySection(name) {
    this.setState({ loading: true });

    requestAPI.get('/general/listaCursosSeccion', {
      params: {
        seccion: name
      }
    }).then(response => {
      this.setState({
        cursos: response.data.cursos,
        cursosAux: response.data.cursos,
        loading: false
      })
    }).catch(error => {
      this.setState({
        loading: false
      });
    });
  }

  search = (text) => {
    let aux = this.state.cursos;
    if (text && text !== '') {//la lista no esta filtrada
      aux = this.state.cursos.filter((d) => {
        return d.nombre.toUpperCase().indexOf(text.toUpperCase()) !== - 1
      });
    }
    this.setState({
      cursosAux: aux
    })
  }

  handleSearchChange = (e) => {
    this.setState({ busqueda: e.target.value })
  }

  render() {
    return (
        <DocumentTitle title="Elpis - Dashboard">
          <PageHeaderLayout title="Cursos" breadcrumbList={[{ title: 'Dashboard' }]}>
            {[Role.JEFE_DEPARTAMENTO, Role.ASISTENTE_DEPARTAMENTO].includes(currentRole()) ?
                <Card style={{ marginBottom: '24px' }}>
                  <Select
                      showSearch
                      placeholder="Select a person"
                      optionFilterProp="children"
                      onChange={this.handleSectionChange}
                      defaultValue="0"
                      style={{ width: '100%' }}
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                    <Option value="0">Todas las secciones</Option>
                    {this.state.secciones.map(s => {
                      return <Option value={s.nombre}>Sección de {s.nombre} </Option>
                    })}
                  </Select>
                </Card>
                : null}
            <Card>
              <Search
                  value={this.state.busqueda}
                  onChange={this.handleSearchChange}
                  placeholder="Buscar"
                  className={styles.search}
                  onSearch={this.search}/>
              <Table
                  rowKey="codigo"
                  columns={columns}
                  dataSource={this.state.cursosAux}
                  pagination={{ pageSize: 6 }}
                  onRow={(record) => {
                    return {
                      onClick: () => {
                      }
                    };
                  }}/>
            </Card>
          </PageHeaderLayout>
        </DocumentTitle>
    )
  }
}