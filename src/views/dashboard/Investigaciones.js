import React from 'react';
import {Badge, Card, Col, Input, Row, Select, Table, Tag} from 'antd';
import {download, requestAPI} from '../../api';
import styles from './Cursos.module.less';
import DocumentTitle from 'react-document-title';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import {currentRole, Role} from '../../util';

const Option = Select.Option;
const Search = Input.Search;

function getLabel(estado) {
  let color = '';
  switch (estado) {
    case 'Pendiente':
      color = 'red';
      break;
    case 'Aprobado':
      color = 'green';
      break;
    case 'En proceso':
      color = 'blue';
      break;
    case 'Finalizada':
      color = 'green';
      break;
  }
  return <Tag color={color}> {estado} </Tag>
}

const columns = [
  {
    key: 'nombre',
    title: 'Nombre',
    width: '40%',
    render: (record) => {
      return (
          <div>
            <b> {record.titulo} </b>
            <small style={{ display: 'block' }}> INV001</small>
          </div>
      )
    }
  }, {
    key: 'creditos',
    title: '',
    align: 'center',
    render: (r) => {
      return (
          <div>
            <div>
              <Badge count={r.profesores.length} style={{ backgroundColor: '#52c41a' }} showZero={true}/>
              <small style={{ display: 'block', marginTop: '5px' }}> investigadores</small>
            </div>
          </div>
      )
    }
  }, {
    key: 'estado',
    title: 'Estado',
    align: 'center',
    render: (r) => {
      return (
          <div>
            {getLabel(r.estado)}
          </div>
      )
    }
  }
];

export default class Cursos extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      busqueda: '',
      secciones: [],
      show: false,
      showResumen: false,
      investigacion: {
        profesores: []
      },
      investigaciones: [],
      aux: [],
      loading: false
    }
  }

  componentDidMount() {
    if ([Role.JEFE_DEPARTAMENTO, Role.ASISTENTE_DEPARTAMENTO].includes(currentRole())) {
      this.allSections();
    }
    this.all();
  }

  allSections() {
    requestAPI.get('/general/listaSeccionesDep')
        .then(response => {
          let aux = response.data.secciones;
          this.setState({
            secciones: aux
          });
        })
  }


  handleSectionChange = (value) => {
    if (value !== '0') {
      this.setState({busqueda: ''});
      this.allBySection(value);
    } else {
      this.all();
    }
  }

  all() {
    this.setState({ loading: true });
    requestAPI.get('/general/listaInvestigacionDep?departamento=Ingenieria')
        .then(response => {
          this.setState({
            investigaciones: response.data.investigaciones,
            aux: response.data.investigaciones,
            loading: false
          })
        }).catch(error => {
      this.setState({
        loading: false
      })
    });
  }


  allBySection(nombre) {
    this.setState({ loading: true });

    requestAPI.get('/general/listaInvestigacionsec', {
      params: {
        seccion: nombre
      }
    }).then(response => {
      this.setState({
        investigaciones: response.data.investigaciones,
        aux: response.data.investigaciones,
        loading: false
      })
    }).catch(error => {
      this.setState({
        loading: false
      })
    });
  }

  search = (text) => {
    let aux = this.state.investigaciones;
    if (text && text !== '') {
      aux = this.state.investigaciones.filter((d) => {
        return d.titulo.toUpperCase().indexOf(text.toUpperCase()) !== - 1
      });
    }
    this.setState({
      aux: aux
    })
  }

  summary = (record) => {
    return (
        <div>
          <div style={{ textAlign: 'justify' }}>{record.resumen}</div>
          <a style={{ display: 'block', fontSize: '14px', marginTop: '5px', marginBottom: '10px' }} onClick={() => {
            download(6)
          }}> Descargar documento </a>
          <b> Investigadores </b>
          {record.profesores.map(p => {
            return (
                <p key={p.codigo}> {p.nombre}</p>
            )
          })}
        </div>
    )
  }

  handleSearchChange = (e) => {
    this.setState({ busqueda: e.target.value })
  }

  render() {

    const responsive = {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 12,
      xl: 6
    };

    return (
        <DocumentTitle title="Elpis - Dashboard">
          <PageHeaderLayout title="Investigaciones" breadcrumbList={[{ title: 'Dashboard' }]}>
            {[Role.JEFE_DEPARTAMENTO, Role.ASISTENTE_DEPARTAMENTO].includes(currentRole()) ?
                <Card style={{ marginBottom: '24px' }}>
                  <Select
                      showSearch
                      placeholder="Select a person"
                      optionFilterProp="children"
                      onChange={this.handleSectionChange}
                      defaultValue="0"
                      style={{ width: '100%' }}
                      filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                    <Option value="0">Todas las secciones</Option>
                    {this.state.secciones.map(s => {
                      return <Option value={s.nombre}>Sección de {s.nombre} </Option>
                    })}
                  </Select>
                </Card>
                : null}
            <Card>
              <Row>
                <Col {...responsive}>
                  <Search
                      value={this.state.busqueda}
                      onChange={this.handleSearchChange}
                      placeholder="Buscar"
                      className={styles.search}
                      onSearch={this.search}/>
                </Col>
              </Row>
              <Table
                  rowKey="id"
                  loading={this.state.loading}
                  columns={columns}
                  expandedRowRender={record => this.summary(record)}
                  defaultExpandAllRows={true}
                  expandRowByClick={true}
                  dataSource={this.state.aux}
                  pagination={{ pageSize: 6 }}/>
            </Card>
          </PageHeaderLayout>
        </DocumentTitle>
    )
  }
}