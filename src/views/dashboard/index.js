import React from 'react';
import ChartCard from './components/ChartCard';
import {Card, Col, message, Row, Select} from 'antd';
import {CartesianGrid, Cell, Legend, Line, LineChart, Pie, PieChart, ResponsiveContainer, Sector, Tooltip, XAxis, YAxis} from 'recharts';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import DocumentTitle from 'react-document-title';
import {requestAPI} from '../../api';
import {currentRole, getUser, Role} from '../../util';

const Option = Select.Option;

function getRandomColor() {
  let letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i ++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
COLORS.push(getRandomColor());
COLORS.push(getRandomColor());
COLORS.push(getRandomColor());
COLORS.push(getRandomColor());


const renderActiveShape = (props) => {
  //console.log('props:', props);
  const {
    cx, cy, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload, percent, value
  } = props;

  return (
      <g>
        <text x={cx} y={cy} dy={- 5} textAnchor="middle" fill={fill}>{payload.name}</text>
        <text x={cx} y={cy} dy={10} textAnchor="middle" fill={fill}>{`Value ${value}`}</text>
        <text x={cx} y={cy} dy={25} textAnchor="middle" fill={fill}>
          {`(Rate ${(percent * 100).toFixed(2)}%)`}
        </text>
        <Sector
            cx={cx}
            cy={cy}
            innerRadius={innerRadius}
            outerRadius={outerRadius}
            startAngle={startAngle}
            endAngle={endAngle}
            fill={fill}
        />
        <Sector
            cx={cx}
            cy={cy}
            startAngle={startAngle}
            endAngle={endAngle}
            innerRadius={outerRadius + 6}
            outerRadius={outerRadius + 10}
            fill={fill}
        />
        <circle r={2} fill={fill} stroke="none"/>
      </g>
  );
};

export default class Dashboard extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      seccion: undefined,
      secciones: [],
      activeIndex: 0,
      activeIndexDTP: 0,
      dataDocentesTipoDepartamento: [],

      activeIndexATD: 0,
      dataActividadesTipoDepartamento: [],

      activeIndexAEED: 0,
      dataApoyoEconomicoEstadoDepartamento: [],

      activeIndexCED: 0,
      dataConvocatoriaEstadoDepartamento: [],

      investigacionesAnho: [],
      apoyoEconomicoAnho: []
    }
  }

  componentDidMount() {
    if ([Role.JEFE_DEPARTAMENTO, Role.ASISTENTE_DEPARTAMENTO].includes(currentRole())) {
      this.allSections();
      this.obtenerDocentesTipoDepartamento();
      this.obtenerActividadesTipoDepartamento();
      this.obtenerApoyoEconomicoEstadoDepartamento();
      this.obtenerConvocatoriaEstadoDepartamento();
      this.obtenerInvestigacionesAnhoDepartamento();
      this.obtenerApoyoEconomicoAnoDepartamento();
    }else {
      let seccion_id = getUser().unidad;
      this.obtenerApoyoEconomicoAnoSeccion(seccion_id);
      this.obtenerDocentesTipoSeccion(seccion_id);
      this.obtenerActividadesTipoSeccion(seccion_id);
      this.obtenerApoyoEconomicoEstadoSeccion(seccion_id);
      this.obtenerConvocatoriaEstadoSeccion(seccion_id);
    }

    this.onPieEnter = this.onPieEnter.bind(this);
    this.onPieEnterDTP = this.onPieEnterDTP.bind(this);
    this.onPieEnterATD = this.onPieEnterATD.bind(this);
    this.onPieEnterAEED = this.onPieEnterAEED.bind(this);
    this.onPieEnterCED = this.onPieEnterCED.bind(this);

  }

  handleSectionChange = (seccion_id) => {
    if (seccion_id !== '0') {
      this.obtenerApoyoEconomicoAnoSeccion(seccion_id);
      this.obtenerDocentesTipoSeccion(seccion_id);
      this.obtenerActividadesTipoSeccion(seccion_id);
      this.obtenerApoyoEconomicoEstadoSeccion(seccion_id);
      this.obtenerConvocatoriaEstadoSeccion(seccion_id);
    } else {
      this.obtenerDocentesTipoDepartamento();
      this.obtenerActividadesTipoDepartamento();
      this.obtenerApoyoEconomicoEstadoDepartamento();
      this.obtenerConvocatoriaEstadoDepartamento();
      this.obtenerInvestigacionesAnhoDepartamento();
      this.obtenerApoyoEconomicoAnoDepartamento();
    }
  }


  allSections() {
    requestAPI.get('/general/listaSeccionesDep')
        .then(response => {
          this.setState({ secciones: response.data.secciones })
        });
  }

  onPieEnter(data, index) {
    this.setState({
      activeIndex: index
    });
  }


  onPieEnterDTP(data, index) {
    this.setState({
      activeIndexDTP: index
    });
  }


  onPieEnterATD(data, index) {
    this.setState({
      activeIndexATD: index
    });
  }

  onPieEnterAEED(data, index) {
    this.setState({
      activeIndexAEED: index
    });
  }

  onPieEnterCED(data, index) {
    this.setState({
      activeIndexCED: index
    });
  }

  obtenerApoyoEconomicoAnoDepartamento() {
    requestAPI.get('/dashboard/apoyoEconomicoAnoDepartamento?anho=2018')
        .then(response => {
          this.setState({ apoyoEconomicoAnho: response.data });
        });
  }

  obtenerInvestigacionesAnhoDepartamento() {
    requestAPI.get('/dashboard/investigacionesAnoDepartamento?anho=2018')
        .then(response => {
          this.setState({ investigacionesAnho: response.data });
        })
  }


  obtenerDocentesTipoDepartamento() {
    requestAPI.get('/dashboard/docentesTipoDepartamento')
        .then(response => {
          this.setState({ dataDocentesTipoDepartamento: response.data });
        });
  }

  obtenerActividadesTipoDepartamento() {
    requestAPI.get('/dashboard/actividadesTipoDepartamento')
        .then(response => {
          this.setState({ dataActividadesTipoDepartamento: response.data });
        });
  }

  obtenerApoyoEconomicoEstadoDepartamento() {
    requestAPI.get('/dashboard/apoyoEconomicoEstadoDepartamento')
        .then(response => {
          this.setState({ dataApoyoEconomicoEstadoDepartamento: response.data });
        });
  }

  obtenerConvocatoriaEstadoDepartamento() {
    requestAPI.get('/dashboard/convocatoriaEstadoDepartamento')
        .then(response => {
          this.setState({ dataConvocatoriaEstadoDepartamento: response.data });
        })
  }

  obtenerApoyoEconomicoAnoSeccion(seccion_id) {
    requestAPI.get('/dashboard/investigacionesAnoSeccion', {
      params: {
        idSeccion: seccion_id,
        anho: 2018
      }
    }).then(response => {
      this.setState({ investigacionesAnho: response.data });
    })
  }

  obtenerDocentesTipoSeccion(seccion_id) {
    requestAPI.get('/dashboard/docentesTipoSeccion', {
      params: {
        idSeccion: seccion_id
      }
    }).then(response => {
      this.setState({ dataDocentesTipoDepartamento: response.data });
    });
  }

  obtenerActividadesTipoSeccion(seccion_id) {
    requestAPI.get('/dashboard/actividadesTipoSeccion', {
      params: {
        idSeccion: seccion_id
      }
    }).then(response => {
      this.setState({ dataActividadesTipoDepartamento: response.data });
    });
  }

  obtenerApoyoEconomicoEstadoSeccion(seccion_id) {
    requestAPI.get('/dashboard/apoyoEconomicoEstadoSeccion', {
      params: {
        idSeccion: seccion_id
      }
    }).then(response => {
      this.setState({ dataApoyoEconomicoEstadoDepartamento: response.data });
    });
  }

  obtenerConvocatoriaEstadoSeccion(seccion_id) {
    requestAPI.get('/dashboard/convocatoriaEstadoSeccion', {
      params: {
        idSeccion: seccion_id
      }
    }).then(response => {
      this.setState({ dataConvocatoriaEstadoDepartamento: response.data });
    });
  }

  render() {

    const topColResponsiveProps = {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 12,
      xl: 6,
      style: { marginBottom: 24 }
    };
    const largeColResponsiveProps = {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 12,
      xl: 12,
      style: { marginBottom: 24 }
    };

    const pieh = '240px';
    const xcenter = 160;
    const ycenter = 115;
    return (
        <div>
          <DocumentTitle title="Elpis - Dashboard">
            <PageHeaderLayout title="Dashboard" breadcrumbList={[]}>
              {[Role.JEFE_DEPARTAMENTO, Role.ASISTENTE_DEPARTAMENTO].includes(currentRole()) ?
                  <Card style={{ marginBottom: '24px' }}>
                    <Select
                        showSearch
                        placeholder="Select a person"
                        optionFilterProp="children"
                        onChange={this.handleSectionChange}
                        defaultValue="0"
                        style={{ width: '100%' }}
                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                      <Option value="0">Todas las secciones</Option>
                      {this.state.secciones.map(s => {
                        return <Option value={s.id}>Sección de {s.nombre} </Option>
                      })}
                    </Select>
                  </Card>
                  : null}
              <Row gutter={24}>
                <Col {...topColResponsiveProps}>
                  <ChartCard title="Convocatorias por Estado">
                    <div style={{ width: '100%', height: pieh }}>
                      <ResponsiveContainer>
                        <PieChart>
                          <Pie
                              activeIndex={this.state.activeIndexCED}
                              activeShape={renderActiveShape}
                              data={this.state.dataConvocatoriaEstadoDepartamento}
                              cx={xcenter}
                              cy={ycenter}
                              innerRadius={60}
                              outerRadius={80}
                              fill="#8884d8"
                              onMouseEnter={this.onPieEnterCED}
                          >
                            {
                              this.state.dataConvocatoriaEstadoDepartamento.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                            }
                          </Pie>
                        </PieChart>
                      </ResponsiveContainer>
                    </div>
                  </ChartCard>
                </Col>
                <Col {...topColResponsiveProps}>
                  <ChartCard title="Profesores por tipo">
                    <div style={{ width: '100%', height: pieh }}>
                      <ResponsiveContainer>
                        <PieChart width={800} height={400}>
                          <Pie
                              activeIndex={this.state.activeIndexDTP}
                              activeShape={renderActiveShape}
                              data={this.state.dataDocentesTipoDepartamento}
                              cx={xcenter}
                              cy={ycenter}
                              innerRadius={60}
                              outerRadius={80}
                              fill="#8884d8"
                              onMouseEnter={this.onPieEnterDTP}
                          >
                            {
                              this.state.dataDocentesTipoDepartamento.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                            }
                          </Pie>
                        </PieChart>
                      </ResponsiveContainer>
                    </div>
                  </ChartCard>
                </Col>
                <Col {...topColResponsiveProps}>
                  <ChartCard title="Solicitudes Economicas por tipo">
                    <div style={{ width: '100%', height: pieh }}>
                      <ResponsiveContainer>
                        <PieChart width={800} height={400}>
                          <Pie
                              activeIndex={this.state.activeIndexAEED}
                              activeShape={renderActiveShape}
                              data={this.state.dataApoyoEconomicoEstadoDepartamento}
                              cx={xcenter}
                              cy={ycenter}
                              innerRadius={60}
                              outerRadius={80}
                              fill="#8884d8"
                              onMouseEnter={this.onPieEnterAEED}
                          >
                            {
                              this.state.dataApoyoEconomicoEstadoDepartamento.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                            }
                          </Pie>
                        </PieChart>
                      </ResponsiveContainer>
                    </div>
                  </ChartCard>
                </Col>
                <Col {...topColResponsiveProps}>
                  <ChartCard title="Actividades por tipo">
                    <div style={{ width: '100%', height: pieh }}>
                      <ResponsiveContainer>
                        <PieChart>
                          <Pie
                              activeIndex={this.state.activeIndexATD}
                              activeShape={renderActiveShape}
                              data={this.state.dataActividadesTipoDepartamento}
                              cx={xcenter}
                              cy={ycenter}
                              innerRadius={60}
                              outerRadius={80}
                              fill="#8884d8"
                              onMouseEnter={this.onPieEnterATD}
                          >
                            {
                              this.state.dataActividadesTipoDepartamento.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                            }
                          </Pie>
                        </PieChart>
                      </ResponsiveContainer>
                    </div>
                  </ChartCard>
                </Col>
                <Col {...largeColResponsiveProps}>
                  <ChartCard title="Actividades por tipo">
                    <div style={{ width: '100%', height: '290px' }}>
                      <ResponsiveContainer>
                        <LineChart data={this.state.apoyoEconomicoAnho}
                                   margin={{ right: 10, left: - 30 }}>
                          <XAxis dataKey="mes"/>
                          <YAxis/>
                          <CartesianGrid strokeDasharray="3 3"/>
                          <Tooltip/>
                          <Legend/>
                          <Line type="monotone" dataKey="apoyo" stroke="#8884d8" activeDot={{ r: 8 }}/>
                        </LineChart>
                      </ResponsiveContainer>
                    </div>
                  </ChartCard>
                </Col>
                <Col {...largeColResponsiveProps}>
                  <ChartCard title="Cantidad de Investigaciones">
                    <div style={{ width: '100%', height: '290px' }}>
                      <ResponsiveContainer>
                        <LineChart data={this.state.investigacionesAnho}
                                   margin={{ right: 10, left: - 30 }}>
                          <XAxis dataKey="mes"/>
                          <YAxis/>
                          <CartesianGrid strokeDasharray="3 3"/>
                          <Tooltip/>
                          <Legend/>
                          <Line type="monotone" dataKey="cantidad" stroke="#8884d8" activeDot={{ r: 8 }}/>
                        </LineChart>
                      </ResponsiveContainer>
                    </div>
                  </ChartCard>
                </Col>
              </Row>
            </PageHeaderLayout>
          </DocumentTitle>
        </div>
    )
  }
}