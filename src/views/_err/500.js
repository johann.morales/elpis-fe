import React from 'react';
import styles from './index.module.less';
import { Button } from 'antd';

export default class Error500 extends React.Component {
  render (){
    const image = 'https://gw.alipayobjects.com/zos/rmsportal/wZcnGqRDyhPOEYFcZDnb.svg';
    const {history} = this.props;
    return (
        <div className={styles.exception}>
          <div className={styles.imgBlock}>
            <div
                className={styles.imgEle}
                style={{ backgroundImage: `url(${ image })` }}
            />
          </div>
          <div className={styles.content}>
            <h1> 500 </h1>
            <div className={styles.desc}> Error del servidor =( </div>
            <div className={styles.actions}>
              <Button type="primary" onClick={() => history.goBack() }> Volver </Button>
            </div>
          </div>
        </div>
    )
  }
}