import React from 'react';
import {Card, Table, Tag} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import DocumentTitle from 'react-document-title';
import {requestAPI} from '../../api';

const columns = [{
  key: '1',
  title: 'NomAAAAAAAAbre',
  width: '25%',
  render: () => {
    return (
        <div>
          <b> Freddy Paz Espinoza </b>
          <small style={{ display: 'block' }}> DNI 74059739</small>
        </div>
    )
  }
}, {
  key: '2',
  title: 'Fecha de Postulación',
  align: 'center',
  render: () => {
    return (
        <div>
          <span> 17/12/1997 </span>
        </div>
    )
  }
}, {
  key: '3',
  title: 'Estado',
  align: 'center',
  render: () => {
    return (
        <div>
          <Tag color="red">Rechazado</Tag>
        </div>
    )
  }
}];

const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York No. 1 Lake Park'
}, {
  key: '2',
  name: 'Jim Green',
  age: 42,
  address: 'London No. 1 Lake Park'
}
];


export default class Postulante extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      'postulante': [
        {
          'id': 2,
          'nombres': 'Darma',
          'apellido_paterno': '',
          'apellido_materno': '',
          'correo': '',
          'fecha_nacimiento': '1994-06-18',
          'numero_documento': 0,
          'tipo_documento': 'Documento Nacional de Identidad',
          'sexo': 'masculino',
          'pais_nacimiento': 'Japón',
          'lugar_nacimiento': 'America Mura ',
          'direccion_domicilio': 'Triangle park  - Osaka',
          'pais_domicilio': 'Japón',
          'telefono': '(+51)111111111',
          'celular': '(+51)1123581317',
          'estado_postulante': 'Pendiente',
          'id_convocatoria': 1
        }
      ],
      'postulante_investigacion': [],
      'postulante_experiencia': [],
      'postulante_docencia_cargo': [],
      'postulante_docencia_premio': [],
      'postulante_docencia_asesoria': [],
      'postulante_grado_titulo': [],
      'postulante_grado_maestria': [],
      'postulante_grado_doctorado': [],
      'postulante_grado_diplomatura': [],
      convocatoria: '',
      breadcrumb:[]
    }
  }


  componentDidMount() {
    requestAPI.get('convocatoria/convocatoria/postulante/devolver', {
      params: {
        id_postulante: this.props.match.params.id_postulante
      }
    }).then(response => {
      this.setState(response.data);
    }).catch(error => {

    });
    this.getBreadcrumb()


  }

  getBreadcrumb = () => {
    requestAPI.get('convocatoria/convocatoria/detalle', {
      params: {
        id: this.props.match.params.id_convocatoria
      }
    }).then(response => {
        this.state.breadcrumb.push({ title: 'Convocatorias', href: '/convocatorias' });
        this.state.breadcrumb.push( { title: response.data[0].nombre, href: '/convocatorias/' + response.data[0].codigo });
    })
  }

  render() {
    const postulante = this.state.postulante;
    return (
        <DocumentTitle title="Elpis - Convocatorias">
          <PageHeaderLayout title={`${postulante[0].nombres} ${postulante[0].apellido_paterno} ${postulante[0].apellido_materno}`} breadcrumbList={this.state.breadcrumb}>
            <Card bordered={false}>
              <Table
                  rowKey={'key'}
                  columns={columns}
                  dataSource={data}
                  onRow={(record) => {
                    return {
                      onClick: () => {
                      }
                    };
                  }}/>
            </Card>
          </PageHeaderLayout>
        </DocumentTitle>
    )
  }
}