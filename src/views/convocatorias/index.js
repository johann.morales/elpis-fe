import React, {Fragment} from 'react';
// import {Table, Card, Tag, Badge} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import DocumentTitle from 'react-document-title';
import {requestAPI} from '../../api';
import {
  Button,
  Menu,
  Dropdown,
  Icon,
  Row,
  Col,
  Steps,
  Card,
  Popover,
  Badge,
  Table,
  Tooltip,
  Divider,
    Tag
} from 'antd';

const ButtonGroup = Button.Group;


export default class Postulante extends React.Component {

  constructor() {
    super();
    this.state = {
      convocatorias: [],
      loading: false
    }
    this.search();
  }

  getTag = (state) => {
    switch (state) {
      case 'Creada':
        return <Tag> Creado </Tag>;
      case 'Aprobada':
        return <Tag color="blue"> Aprobada </Tag>;
      case 'Abierta':
        return <Tag color="green"> Abierta </Tag>;
      case 'Cerrada':
        return <Tag color="label label-danger"> Cerrado </Tag>;
      case 'Cancelada':
        return <Tag color="label label-danger"> Cancelado </Tag>;
      case 'Finalizada':
        return <Tag color="label label-success"> Finalizado </Tag>;
      default:
        return <span></span>;
    }
  }


  search = () => {
    this.setState({loading: true})
    requestAPI.get('convocatoria/convocatoria/lista')
        .then(response => {
          this.setState({ convocatorias: response.data.convocatorias, loading: false })
        })
  }

  render() {
    const { match, history } = this.props;
    const breadcrumbList = [];

    const columns = [{
      key: 'nombre',
      title: 'Nombre',
      width: '25%',
      render: (r) => {
        return (
            <div>
              <b> {r.nombre} </b>
              <small style={{ display: 'block' }}> CONV00{r.codigo}</small>
            </div>
        )
      }
    }, {
      key: 'seccion',
      title: 'Sección',
      align: 'center',
      render: () => {
        return (
            <div>
              <span> Secc. de Ingeniería Informática </span>
              <small style={{ display: 'block' }}> Dpto. de Ciencias e Ingeniería </small>
            </div>
        )
      }
    }, {
      key:'postulantes',
      title: '',
      align: 'center',
      render: (r) => {
        return (
            <div>
              <Badge count={r.cantidadPostulantes} style={{ backgroundColor: '#fff', color: '#999', boxShadow: '0 0 0 1px #d9d9d9 inset' }} showZero={true}/>
              <small style={{ display: 'block', marginTop: '5px' }}> postulantes</small>
            </div>
        )
      }
    }, {
      key: 'estado',
      title: 'Estado',
      align: 'center',
      render: (r) => {
        return (
            <div>
              {this.getTag(r.estado)}
            </div>
        )
      }
    }];
    const action = (
        <Fragment>
          <ButtonGroup>
            <Button type="primary">Aceptar</Button>
            <Button type="error">Rechazar</Button>
          </ButtonGroup>
          <Button type="primary">Acciones</Button>
        </Fragment>
    );
    return (

        <DocumentTitle title="Elpis - Convocatorias">
          <PageHeaderLayout title="Convocatorias" breadcrumbList={breadcrumbList} action={action}>
            <Card bordered={false}>
              <Table
                  striped
                  rowKey="id"
                  loading={this.state.loading}
                  columns={columns}
                  dataSource={this.state.convocatorias}
                  onRow={(record) => {
                    return {
                      onClick: () => {
                        history.push(`${match.url}/${record.id}`)
                      }
                    };
                  }}/>
            </Card>
          </PageHeaderLayout>
        </DocumentTitle>
    )
  }
}