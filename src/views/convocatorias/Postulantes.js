import React from 'react';
import {Card, Col, Row, Table, Tag, Input} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import DocumentTitle from 'react-document-title';
import styles from './Postulantes.module.less';
import {requestAPI} from '../../api';

const Search = Input.Search;

export default class Postulantes extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nombre: '',
      postulantes: [],
      aux: []
    }
    this.search();
  }

  getTag = (state) => {
    switch (state) {
      case 'Pendiente':
        return <Tag> Pendiente </Tag>;
      case 'Aceptado':
        return <Tag color="green"> Aceptado </Tag>;
      case 'Rechazado':
        return <Tag color="red"> Rechazado </Tag>;
      default:
        return <span></span>;
    }
  }

  search = () => {
    requestAPI.get('convocatoria/convocatoria/detalle', {
      params: {
        id: this.props.match.params.id_convocatoria
      }
    }).then(response => {
      this.setState(response.data[0]);
      this.setState({ aux: response.data[0].postulantes });
    })
  }

  buscar = (text) => {
    let aux = this.state.postulantes;

    if (text && text !== '') {
      aux = this.state.postulantes.filter((d) => {
        return d.nombre.toUpperCase().indexOf(text.toUpperCase()) !== - 1
      });
    }
    this.setState({
      aux: aux
    })
  }

  render() {

    const columns = [{
      key:"nombre",
      title: 'Nombre',
      width: '25%',
      render: (r) => {
        return (
            <div>
              <b> {r.nombre} </b>
              <small style={{ display: 'block' }}> DNI 74059739</small>
            </div>
        )
      }
    }, {
      key:"fecha",
      title: 'Fecha de Postulación',
      align: 'center',
      render: (r) => {
        return (
            <div>
              <span> {r.fecha_postulacion} </span>
            </div>
        )
      }
    }, {
      key:"estado",
      title: 'Estado',
      align: 'center',
      render: (r) => {
        return (
            <div>
              {this.getTag(r.estado_postulacion)}
            </div>
        )
      }
    }];

    const Info = ({ title, value, bordered }) => (
        <div className={styles.headerInfo}>
          <span>{title}</span>
          <p>{value}</p>
          {bordered && <em/>}
        </div>
    );

    const { match, history } = this.props;
    const breadcrumbList = [
      { title: 'Convocatorias', href: '/convocatorias' }
    ];


    return (
        <DocumentTitle title="Elpis - Convocatorias">
          <PageHeaderLayout title={this.state.nombre} breadcrumbList={breadcrumbList}>
            <div className={styles.standardList}>
              <Card bordered={false}>
                <Row>
                  <Col sm={8} xs={24}>
                    <Info title="Pendientes " value={this.state.postulantes.filter(p => p.estado_postulacion === 'Pendiente').length} bordered/>
                  </Col>
                  <Col sm={8} xs={24}>
                    <Info title="Rechazados" value={this.state.postulantes.filter(p => p.estado_postulacion === 'Rechazado').length} bordered/>
                  </Col>
                  <Col sm={8} xs={24}>
                    <Info title="Aceptados" value={this.state.postulantes.filter(p => p.estado_postulacion === 'Aceptado').length}/>
                  </Col>
                </Row>
              </Card>
              <Card bordered={false}>
                <Search
                    placeholder="Buscar"
                    className={styles.search}
                    onSearch={this.buscar}/>
                <Table
                    rowKey="codigo"
                    columns={columns}
                    dataSource={this.state.aux}
                    pagination={{ pageSize: 8 }}
                    onRow={(record) => {
                      return {
                        onClick: () => {
                          history.push(`${match.url}/postulantes/${record.codigo}`);
                        }
                      };
                    }}/>
              </Card>
            </div>
          </PageHeaderLayout>
        </DocumentTitle>
    )
  }
}