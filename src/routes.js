import {Role} from './util'

const all = [Role.ASISTENTE_DEPARTAMENTO, Role.ASISTENTE_SECCION, Role.JEFE_DEPARTAMENTO, Role.COORDINADOR];

export default [
  {
    show: false,
    authorization: false,
    path: {
      name: '/login',
      isExact: true
    },
    layout: require('./layouts/LayoutBlank'),
    component: require('./views/auth/SignIn')
  },
  {
    show: false,
    authorization: false,
    path: {
      name: '/signup',
      isExact: true
    },
    layout: require('./layouts/LayoutBlank'),
    component: require('./views/auth/SignUp')
  },
  {
    show: true,
    authorization: true,
    roles: all,
    name: 'Cursos',
    path: {
      name: '/dashboard/cursos',
    },
    component: require('./views/dashboard/Cursos')
  },
  {
    show: true,
    authorization: true,
    roles: all,
    name: 'Investigaciones',
    path: {
      name: '/dashboard/investigaciones',
    },
    component: require('./views/dashboard/Investigaciones')
  },
  {
    show: true,
    authorization: true,
    roles: all,
    icon: 'line-chart',
    name: 'Dashboard',
    path: {
      name: '/dashboard',
      isExact: true
    },
    layout: require('./layouts/LayoutBasic'),
    component: require('./views/dashboard')
  },
  {
    show: true,
    authorization: true,
    roles: all,
    icon: 'user',
    name: 'Profesores',
    path: {
      name: '/profesores',
      isExact: true
    },
    component: require('./views/profesores')
  },
  {
    show: false,
    authorization: true,
    roles: all,
    path: {
      name: '/convocatorias/:id_convocatoria/postulantes/:id_postulante',
      isExact: true
    },
    component: require('./views/convocatorias/Postulante')
  },
  {
    show: false,
    authorization: true,
    roles: all,
    path: {
      name: '/convocatorias/:id_convocatoria',
      isExact: true
    },
    component: require('./views/convocatorias/Postulantes')
  },
  {
    show: true,
    authorization: true,
    roles: all,
    icon: 'calendar',
    name: 'Convocatorias',
    path: {
      name: '/convocatorias',
      isExact: true
    },
    component: require('./views/convocatorias')
  },
  {
    show: false,
    authorization: true,
    roles: all,
    path: {
      name: '/solicitudes/:id_solicitud/aprobar',
      isExact: true
    },
    component: require('./views/solicitudes/SolicitudesAprobar')
  },
  {
    show: false,
    authorization: true,
    roles: all,
    path: {
      name: '/solicitudes/:id_solicitud/gastos',
      isExact: true
    },
    component: require('./views/solicitudes/SolicitudesGastos')
  },
  {
    show: true,
    authorization: true,
    roles: all,
    icon: 'wallet',
    name: 'Solicitudes Económicas',
    path: {
      name: '/solicitudes',
      isExact: true
    },
    component: require('./views/solicitudes')
  },
  {
    show: true,
    authorization: true,
    roles: [Role.ASISTENTE_DEPARTAMENTO, Role.JEFE_DEPARTAMENTO],
    icon: 'form',
    name: 'Preferencia de Dictado',
    path: {
      name: '/preferencias',
      isExact: true
    },
    component: require('./views/preferencias')
  },
  {
    show: true,
    authorization: true,
    roles: [Role.ASISTENTE_DEPARTAMENTO, Role.JEFE_DEPARTAMENTO],
    icon: 'upload',
    name: 'Carga de Datos',
    path: {
      name: '/cargadatos',
      isExact: true
    },
    component: require('./views/dashboard')
  },
]

