import React, {Component} from 'react';
import {BrowserRouter} from 'react-router-dom';

import ApplicationRouter from './router/ApplicationRouter';

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <ApplicationRouter/>
        </BrowserRouter>
    );
  }
}

export default App;